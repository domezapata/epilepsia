// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
   apiKey: "AIzaSyAZjnvfZa89tOtOeglexqoLwDYisR7mhaM",
   authDomain: "epilepsycontrol-4dbc5.firebaseapp.com",
   projectId: "epilepsycontrol-4dbc5",
   storageBucket: "epilepsycontrol-4dbc5.appspot.com",
   messagingSenderId: "152747840574",
   appId: "1:152747840574:web:d4b329ccc56aa0b61aed6f"
 }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
