import { Injectable } from '@angular/core';
import { OneSignal } from '@ionic-native/onesignal/ngx';

@Injectable({
  providedIn: 'root'
})
export class PushService {

  constructor(private oneSignal: OneSignal) { }


  configuracionInicial() {
    this.oneSignal.startInit('0729b80b-8a5b-493e-8079-6d5495399b99', '152747840574');

    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);

    this.oneSignal.handleNotificationReceived().subscribe(( noti) => {
      // do something when notification is received
      console.log("Recibido", noti)
    });

    this.oneSignal.handleNotificationOpened().subscribe(( noti ) => {
      // do something when a notification is opened
      console.log("Abierto", noti)
    });

    this.oneSignal.endInit();
  }
}
