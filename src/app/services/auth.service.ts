import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { AlertController } from '@ionic/angular';
import firebase from 'firebase/app';
import 'firebase/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthService {


  constructor(private alertCtrl: AlertController, private afAuth: AngularFireAuth, private firestore: AngularFirestore,) { }
//Funciones para obtener la uid
  getUser(): firebase.User {
    return firebase.auth().currentUser;
  }

  get Session() {
    return firebase.auth().currentUser;
  }
//login 
  loginUser(
    email: string,
    password: string
  ): Promise<firebase.auth.UserCredential> {

    return firebase.auth().signInWithEmailAndPassword(email, password);
  }

//Registrar usuario
  async signupUser(name: string, typeid: string, nroid: string, gender: string, role: string, address: string, birthdaydate: string, number: string, email: string, password: string, img: string, category:  String, lat:String, log:String): Promise<any> {

    try {
      const newUserCredential: firebase.auth.UserCredential = await firebase.auth().createUserWithEmailAndPassword(email, password);
      await this.firestore.doc(`users/${newUserCredential.user.uid}`).set({ name, typeid, nroid, gender, role, address, birthdaydate, number, email, img,category,lat,log  });


      await newUserCredential.user.sendEmailVerification();

      return newUserCredential;
    } catch (error) {
      throw error;
    }

  }

//actualizacion del usuario
  async updateUser(name: string, typeid: string, nroid: string, gender: string, role: string, address: string, birthdaydate: string, number: string, email: string, img: string, category:  String, lat:String, log:String): Promise<any> {

    try {

      await this.firestore.doc(`users/${this.Session.uid}`).set({ name, typeid, nroid, gender, role, address, birthdaydate, number, email, img,category,lat,log });


    } catch (error) {
      throw error;
    }

  }

  
  async updateprivi(id:String,name: string, typeid: string, nroid: string, gender: string, role: string, address: string, birthdaydate: string, number: string, email: string, img: string, category:  String, lat:String, log:String): Promise<any> {

    try {

      await this.firestore.doc('users/'+id).set({ name, typeid, nroid, gender, role, address, birthdaydate, number, email, img,category,lat,log });


    } catch (error) {
      throw error;
    }

  }


  //inset vehicle

 async insetvehicle(id:String, brand: string, model: string, plate: string, color: string,km:string): Promise<any> {

    try {

      await this.firestore.doc('vehicle/'+id).set({ brand, model, plate, color,km });


    } catch (error) {
      throw error;
    }

  }

//Resetear contraseña
  resetPassword(email: string): Promise<void> {
    return firebase.auth().sendPasswordResetEmail(email);
  }
//Crrrara sesión
  logoutUser(): Promise<void> {
    return firebase.auth().signOut();
  }
  //si esta logeado el usuario
  getauthenticated(): boolean {
    return firebase.auth().currentUser !== null;

  }

  //login con google
  onLoginGoogle() {
    try {
      return this.afAuth.signInWithPopup(
        new firebase.auth.GoogleAuthProvider()

      )
    } catch (error) {
      console.log("Error => " + error)
    }


  }


}