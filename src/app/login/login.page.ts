import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, LoadingController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import 'firebase/auth';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  type: string;
  isActiveToggleTextPassword: Boolean = true;
  password_type: string = 'password';
  eye_type: string = 'eye';


  user = {
    name: '',
    typeid: '',
    nroid: '',
    gender: '',
    role: 'cliente',
    address: '',
    birthdaydate: '',
    email: '',
    password: '',
    confpassword: '',
    category: '',
    number: '',
    img: 'https://firebasestorage.googleapis.com/v0/b/palmago-45e5a.appspot.com/o/default-user.png?alt=media&token=f3e1a1a3-9532-48ac-a2aa-ebaeca5f5ce2',
    lat: '',
    log: '',
  };
  validador: boolean;
  constructor(private authService: AuthService,
    private alertCtrl: AlertController,
    private router: Router, public loadingController: LoadingController) { }

  //Visualizar contrase 
  togglePasswordMode() {
    this.eye_type = this.eye_type === 'eye' ? 'eye-off' : 'eye';
    this.password_type = this.password_type === 'text' ? 'password' : 'text';
  }
  //Inicar seccion  
  async loginUser() {
    //Loading de circulo cargando

    if (this.user.email != '' && this.user.password != '') {
      const loading = await this.loadingController.create({
        cssClass: 'my-custom-class',
        message: 'Por  favor espere...',

      });
      await loading.present();
      console.log("Ingreso");
      this.authService.loginUser(this.user.email, this.user.password).then(
        () => {
          loading.dismiss();
          this.router.navigateByUrl('menu');
        },
        async error => {
          loading.dismiss();
          const alert = await this.alertCtrl.create({
            message: "No hay ningún registro de usuario que corresponda a este identificador.",
            buttons: [{ text: 'Aceptar', role: 'cancel' }],
          });
          await alert.present();
        }
      );
    } else {
      this.error("Inicio sesión", "Por favor ingrese todos los datos");
    }

  }

  //Registrarse usuarios
  async signin() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Por  favor espere...',

    });
    if (this.user.email != '' && this.user.password != ''
      && this.user.name != '' && this.user.role != '') {

      if (this.validar_clave(this.user.password)) {
        //Loading de circulo cargando

        await loading.present();
        this.authService.signupUser(this.user.name, this.user.typeid, this.user.nroid, this.user.gender, this.user.role, this.user.address, this.user.birthdaydate, this.user.number, this.user.email, this.user.password, this.user.img, this.user.category, this.user.lat, this.user.log).then(
          () => {
            loading.dismiss();
            this.router.navigateByUrl('login');
            this.type = "login";
          },
          async error => {
            loading.dismiss();
            const alert = await this.alertCtrl.create({
              message: "La dirección de correo electrónico ya está siendo utilizada por otra cuenta.",
              buttons: [{ text: 'Aceptar', role: 'cancel' }],
            });
            console.log("error " + error);
            await alert.present();
          }
        );
      }
      else {
        this.error("Registro", "Tu contraseña debe tener al menos 8 letras, numeros y simbolos como (! y &)");
      }
    } else {
      loading.dismiss();
      this.error("Registro", "Por favor ingrese todos los campos");
    }

  }
  validar_clave(contrasenna) {
    if (contrasenna.length >= 8) {
      var mayuscula = false;
      var minuscula = false;
      var numero = false;
      var caracter_raro = false;

      for (var i = 0; i < contrasenna.length; i++) {
        if (contrasenna.charCodeAt(i) >= 65 && contrasenna.charCodeAt(i) <= 90) {
          mayuscula = true;
        }
        else if (contrasenna.charCodeAt(i) >= 97 && contrasenna.charCodeAt(i) <= 122) {
          minuscula = true;
        }
        else if (contrasenna.charCodeAt(i) >= 48 && contrasenna.charCodeAt(i) <= 57) {
          numero = true;
        }
        else {
          caracter_raro = true;
        }
      }
      if (mayuscula == true && minuscula == true && caracter_raro == true && numero == true) {
        return true;
      }
    }
    return false;
  }

  ngOnInit() {
    //Variable para iniciar con el  ion-segment
    this.type = "login";

  }
  segmentChanged(ev: any) {
    //valor que se escoge cuando se cambia la opcion en  ion-segment
    console.log('Segment changed', ev);
  }
  //Mensaje de error
  async error(title: string, messag: string) {
    const alert = await this.alertCtrl.create({
      message: messag,
      subHeader: title,
      buttons: [{ text: 'Aceptar', role: 'cancel' }],
    });
    await alert.present();
  }
}