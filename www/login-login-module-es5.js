(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["login-login-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.page.html":
    /*!*****************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.page.html ***!
      \*****************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppLoginLoginPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-grid class=\"grid1\">\r\n\r\n    <ion-row class=\"row1\">\r\n        <ion-col>\r\n            <img src=\"../../assets/imagenes/fondoMain.png\"/>\r\n        </ion-col>\r\n    </ion-row>\r\n\r\n\r\n</ion-grid>\r\n  <ion-slide>\r\n    <div class=\"contaier-login\">\r\n        <ion-grid>\r\n          <ion-row>\r\n            <ion-col>\r\n              <img src=\"../../assets/imagenes/fondoMain.png\"  style=\"align:center width: 100px; height: 150px;\" />\r\n          </ion-col>\r\n          </ion-row>\r\n            <ion-row>\r\n                <ion-col>\r\n                  \r\n                    <h1>Iniciar Sesión</h1>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row>\r\n                <ion-col>\r\n                    <ion-list>\r\n\r\n                        <ion-item>\r\n                            <ion-icon name=\"person\" class=\"icon-login\"></ion-icon>\r\n                            <ion-input name=\"user\" type=\"text\" [(ngModel)]=\"user.email\"\r\n                                placeholder=\"Correo electrónico\" required></ion-input>\r\n                        </ion-item>\r\n                        <br />\r\n                        <ion-item>\r\n                            <ion-icon name=\"key\" class=\"icon-login\"></ion-icon>\r\n                            <ion-input [type]=\"password_type\" [(ngModel)]=\"user.password\" placeholder=\"Contraseña\" name=\"password\"\r\n                                 required></ion-input>\r\n                            <ion-icon name=\"eye\" item-right ></ion-icon>\r\n                        </ion-item>\r\n\r\n                    </ion-list>\r\n                </ion-col>\r\n            </ion-row>\r\n            <ion-row>\r\n                <ion-col>\r\n                    <ion-button (click)=\"loginUser()\" fill=\"solid\" expand=\"full\">\r\n                        <ion-icon name=\"log-in\"></ion-icon>\r\n                        Entrar\r\n                    </ion-button>\r\n                </ion-col>\r\n            </ion-row>\r\n\r\n\r\n            <ion-row>\r\n                <ion-col>\r\n                    <ion-label>\r\n                        <a routerLink=\"/login\">\r\n                            <h3>¿Olvido la contraseña?</h3>\r\n                        </a>\r\n\r\n                    </ion-label>\r\n                </ion-col>\r\n            </ion-row>\r\n\r\n\r\n            <ion-row>\r\n                <ion-col>\r\n                    <ion-label>\r\n                        <h3>¿No tiene cuenta? <a routerLink=\"/registro\">Registrate</a> </h3>\r\n                    </ion-label>\r\n                </ion-col>\r\n            </ion-row>\r\n\r\n        </ion-grid>\r\n    </div>\r\n</ion-slide>\r\n\r\n</ion-content>\r\n";
      /***/
    },

    /***/
    "./src/app/login/login-routing.module.ts":
    /*!***********************************************!*\
      !*** ./src/app/login/login-routing.module.ts ***!
      \***********************************************/

    /*! exports provided: LoginPageRoutingModule */

    /***/
    function srcAppLoginLoginRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPageRoutingModule", function () {
        return LoginPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _login_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./login.page */
      "./src/app/login/login.page.ts");

      var routes = [{
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_3__["LoginPage"]
      }];

      var LoginPageRoutingModule = function LoginPageRoutingModule() {
        _classCallCheck(this, LoginPageRoutingModule);
      };

      LoginPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], LoginPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/login/login.module.ts":
    /*!***************************************!*\
      !*** ./src/app/login/login.module.ts ***!
      \***************************************/

    /*! exports provided: LoginPageModule */

    /***/
    function srcAppLoginLoginModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPageModule", function () {
        return LoginPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _login_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./login-routing.module */
      "./src/app/login/login-routing.module.ts");
      /* harmony import */


      var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./login.page */
      "./src/app/login/login.page.ts");

      var LoginPageModule = function LoginPageModule() {
        _classCallCheck(this, LoginPageModule);
      };

      LoginPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _login_routing_module__WEBPACK_IMPORTED_MODULE_5__["LoginPageRoutingModule"]],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
      })], LoginPageModule);
      /***/
    },

    /***/
    "./src/app/login/login.page.scss":
    /*!***************************************!*\
      !*** ./src/app/login/login.page.scss ***!
      \***************************************/

    /*! exports provided: default */

    /***/
    function srcAppLoginLoginPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".grid1 {\n  height: 100%;\n  padding: 0px;\n}\n\n.contaier-login {\n  width: 90%;\n  padding: 10px;\n  background: #FFFFFF;\n  border-radius: 5px;\n  box-shadow: 0px 0px 10px 1px rgba(0, 0, 0, 0.75);\n}\n\n.btn_eye_icon {\n  position: absolute;\n  right: 0;\n  bottom: 4px;\n  background: transparent;\n}\n\n.btn_eye_icon ion-icon {\n  font-size: 22px;\n  color: #757575;\n}\n\n.btn_eye_icon:focus {\n  outline: none !important;\n}\n\n.row2 {\n  height: 50%;\n}\n\nion-icon {\n  margin-right: 15px;\n}\n\nion-button {\n  margin-top: 5px;\n  margin-bottom: 15px;\n}\n\nion-slide {\n  width: 100%;\n  height: 100%;\n  position: absolute !important;\n  top: 0px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9naW4vbG9naW4ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksWUFBQTtFQUNBLFlBQUE7QUFDSjs7QUFFQTtFQUNJLFVBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUdBLGdEQUFBO0FBQ0o7O0FBQ0E7RUFDSSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxXQUFBO0VBQ0EsdUJBQUE7QUFFSjs7QUFESTtFQUNJLGVBQUE7RUFDQSxjQUFBO0FBR1I7O0FBQ0E7RUFDSSx3QkFBQTtBQUVKOztBQUVBO0VBQ0ksV0FBQTtBQUNKOztBQUVBO0VBQ0ksa0JBQUE7QUFDSjs7QUFFQTtFQUNJLGVBQUE7RUFDQSxtQkFBQTtBQUNKOztBQUVBO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSw2QkFBQTtFQUNBLG1CQUFBO0FBQ0oiLCJmaWxlIjoic3JjL2FwcC9sb2dpbi9sb2dpbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZ3JpZDEge1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgcGFkZGluZzogMHB4O1xyXG59XHJcblxyXG4uY29udGFpZXItbG9naW4ge1xyXG4gICAgd2lkdGg6IDkwJTtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICBiYWNrZ3JvdW5kOiAjRkZGRkZGO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggMHB4IDEwcHggMXB4IHJnYmEoMCwgMCwgMCwgMC43NSk7XHJcbiAgICAtbW96LWJveC1zaGFkb3c6IDBweCAwcHggMTBweCAxcHggcmdiYSgwLCAwLCAwLCAwLjc1KTtcclxuICAgIGJveC1zaGFkb3c6IDBweCAwcHggMTBweCAxcHggcmdiYSgwLCAwLCAwLCAwLjc1KTtcclxufVxyXG4uYnRuX2V5ZV9pY29ue1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgcmlnaHQ6IDA7XHJcbiAgICBib3R0b206IDRweDtcclxuICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG4gICAgaW9uLWljb257XHJcbiAgICAgICAgZm9udC1zaXplOiAyMnB4O1xyXG4gICAgICAgIGNvbG9yOiM3NTc1NzU7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5idG5fZXllX2ljb246Zm9jdXN7XHJcbiAgICBvdXRsaW5lOiBub25lICAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5cclxuLnJvdzIge1xyXG4gICAgaGVpZ2h0OiA1MCU7XHJcbn1cclxuXHJcbmlvbi1pY29uIHtcclxuICAgIG1hcmdpbi1yaWdodDogMTVweDtcclxufVxyXG5cclxuaW9uLWJ1dHRvbiB7XHJcbiAgICBtYXJnaW4tdG9wOiA1cHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xyXG59XHJcblxyXG5pb24tc2xpZGUge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGUgIWltcG9ydGFudDtcclxuICAgIHRvcDogMHB4ICFpbXBvcnRhbnQ7XHJcbn0iXX0= */";
      /***/
    },

    /***/
    "./src/app/login/login.page.ts":
    /*!*************************************!*\
      !*** ./src/app/login/login.page.ts ***!
      \*************************************/

    /*! exports provided: LoginPage */

    /***/
    function srcAppLoginLoginPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPage", function () {
        return LoginPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../services/auth.service */
      "./src/app/services/auth.service.ts");
      /* harmony import */


      var firebase_auth__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! firebase/auth */
      "./node_modules/firebase/auth/dist/index.esm.js");

      var LoginPage = /*#__PURE__*/function () {
        function LoginPage(authService, alertCtrl, router, loadingController) {
          _classCallCheck(this, LoginPage);

          this.authService = authService;
          this.alertCtrl = alertCtrl;
          this.router = router;
          this.loadingController = loadingController;
          this.isActiveToggleTextPassword = true;
          this.password_type = 'password';
          this.eye_type = 'eye';
          this.user = {
            name: '',
            typeid: '',
            nroid: '',
            gender: '',
            role: 'cliente',
            address: '',
            birthdaydate: '',
            email: '',
            password: '',
            confpassword: '',
            category: '',
            number: '',
            img: 'https://firebasestorage.googleapis.com/v0/b/palmago-45e5a.appspot.com/o/default-user.png?alt=media&token=f3e1a1a3-9532-48ac-a2aa-ebaeca5f5ce2',
            lat: '',
            log: ''
          };
        } //Visualizar contrase 


        _createClass(LoginPage, [{
          key: "togglePasswordMode",
          value: function togglePasswordMode() {
            this.eye_type = this.eye_type === 'eye' ? 'eye-off' : 'eye';
            this.password_type = this.password_type === 'text' ? 'password' : 'text';
          } //Inicar seccion  

        }, {
          key: "loginUser",
          value: function loginUser() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var _this = this;

              var loading;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      if (!(this.user.email != '' && this.user.password != '')) {
                        _context2.next = 10;
                        break;
                      }

                      _context2.next = 3;
                      return this.loadingController.create({
                        cssClass: 'my-custom-class',
                        message: 'Por  favor espere...'
                      });

                    case 3:
                      loading = _context2.sent;
                      _context2.next = 6;
                      return loading.present();

                    case 6:
                      console.log("Ingreso");
                      this.authService.loginUser(this.user.email, this.user.password).then(function () {
                        loading.dismiss();

                        _this.router.navigateByUrl('menu');
                      }, function (error) {
                        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                          var alert;
                          return regeneratorRuntime.wrap(function _callee$(_context) {
                            while (1) {
                              switch (_context.prev = _context.next) {
                                case 0:
                                  loading.dismiss();
                                  _context.next = 3;
                                  return this.alertCtrl.create({
                                    message: "No hay ningún registro de usuario que corresponda a este identificador.",
                                    buttons: [{
                                      text: 'Aceptar',
                                      role: 'cancel'
                                    }]
                                  });

                                case 3:
                                  alert = _context.sent;
                                  _context.next = 6;
                                  return alert.present();

                                case 6:
                                case "end":
                                  return _context.stop();
                              }
                            }
                          }, _callee, this);
                        }));
                      });
                      _context2.next = 11;
                      break;

                    case 10:
                      this.error("Inicio sesión", "Por favor ingrese todos los datos");

                    case 11:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          } //Registrarse usuarios

        }, {
          key: "signin",
          value: function signin() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
              var _this2 = this;

              var loading;
              return regeneratorRuntime.wrap(function _callee4$(_context4) {
                while (1) {
                  switch (_context4.prev = _context4.next) {
                    case 0:
                      _context4.next = 2;
                      return this.loadingController.create({
                        cssClass: 'my-custom-class',
                        message: 'Por  favor espere...'
                      });

                    case 2:
                      loading = _context4.sent;

                      if (!(this.user.email != '' && this.user.password != '' && this.user.name != '' && this.user.role != '')) {
                        _context4.next = 13;
                        break;
                      }

                      if (!this.validar_clave(this.user.password)) {
                        _context4.next = 10;
                        break;
                      }

                      _context4.next = 7;
                      return loading.present();

                    case 7:
                      this.authService.signupUser(this.user.name, this.user.typeid, this.user.nroid, this.user.gender, this.user.role, this.user.address, this.user.birthdaydate, this.user.number, this.user.email, this.user.password, this.user.img, this.user.category, this.user.lat, this.user.log).then(function () {
                        loading.dismiss();

                        _this2.router.navigateByUrl('login');

                        _this2.type = "login";
                      }, function (error) {
                        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
                          var alert;
                          return regeneratorRuntime.wrap(function _callee3$(_context3) {
                            while (1) {
                              switch (_context3.prev = _context3.next) {
                                case 0:
                                  loading.dismiss();
                                  _context3.next = 3;
                                  return this.alertCtrl.create({
                                    message: "La dirección de correo electrónico ya está siendo utilizada por otra cuenta.",
                                    buttons: [{
                                      text: 'Aceptar',
                                      role: 'cancel'
                                    }]
                                  });

                                case 3:
                                  alert = _context3.sent;
                                  console.log("error " + error);
                                  _context3.next = 7;
                                  return alert.present();

                                case 7:
                                case "end":
                                  return _context3.stop();
                              }
                            }
                          }, _callee3, this);
                        }));
                      });
                      _context4.next = 11;
                      break;

                    case 10:
                      this.error("Registro", "Tu contraseña debe tener al menos 8 letras, numeros y simbolos como (! y &)");

                    case 11:
                      _context4.next = 15;
                      break;

                    case 13:
                      loading.dismiss();
                      this.error("Registro", "Por favor ingrese todos los campos");

                    case 15:
                    case "end":
                      return _context4.stop();
                  }
                }
              }, _callee4, this);
            }));
          }
        }, {
          key: "validar_clave",
          value: function validar_clave(contrasenna) {
            if (contrasenna.length >= 8) {
              var mayuscula = false;
              var minuscula = false;
              var numero = false;
              var caracter_raro = false;

              for (var i = 0; i < contrasenna.length; i++) {
                if (contrasenna.charCodeAt(i) >= 65 && contrasenna.charCodeAt(i) <= 90) {
                  mayuscula = true;
                } else if (contrasenna.charCodeAt(i) >= 97 && contrasenna.charCodeAt(i) <= 122) {
                  minuscula = true;
                } else if (contrasenna.charCodeAt(i) >= 48 && contrasenna.charCodeAt(i) <= 57) {
                  numero = true;
                } else {
                  caracter_raro = true;
                }
              }

              if (mayuscula == true && minuscula == true && caracter_raro == true && numero == true) {
                return true;
              }
            }

            return false;
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {
            //Variable para iniciar con el  ion-segment
            this.type = "login";
          }
        }, {
          key: "segmentChanged",
          value: function segmentChanged(ev) {
            //valor que se escoge cuando se cambia la opcion en  ion-segment
            console.log('Segment changed', ev);
          } //Mensaje de error

        }, {
          key: "error",
          value: function error(title, messag) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
              var alert;
              return regeneratorRuntime.wrap(function _callee5$(_context5) {
                while (1) {
                  switch (_context5.prev = _context5.next) {
                    case 0:
                      _context5.next = 2;
                      return this.alertCtrl.create({
                        message: messag,
                        subHeader: title,
                        buttons: [{
                          text: 'Aceptar',
                          role: 'cancel'
                        }]
                      });

                    case 2:
                      alert = _context5.sent;
                      _context5.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context5.stop();
                  }
                }
              }, _callee5, this);
            }));
          }
        }]);

        return LoginPage;
      }();

      LoginPage.ctorParameters = function () {
        return [{
          type: _services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"]
        }];
      };

      LoginPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./login.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./login.page.scss */
        "./src/app/login/login.page.scss"))["default"]]
      })], LoginPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=login-login-module-es5.js.map