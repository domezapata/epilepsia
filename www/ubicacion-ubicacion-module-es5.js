(function () {
  function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

  function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

  function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

  function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

  function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

  function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["ubicacion-ubicacion-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/ubicacion/ubicacion.page.html":
    /*!*************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/ubicacion/ubicacion.page.html ***!
      \*************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppUbicacionUbicacionPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-back-button slot=\"start\" defaultHref=\"menu\"></ion-back-button>\r\n    <ion-title>Ubicación</ion-title>\r\n  </ion-toolbar> \r\n  <ion-row>    \r\n    <ion-col size=\"6\">\r\n      <ion-text>Encontrar ubicación</ion-text>\r\n    </ion-col>\r\n    <ion-col  size=\"6\">\r\n      <ion-button (click)=\"loadMap()\" shape=\"round\" fill=\"outline\">\r\n        <ion-icon slot=\"start\" name=\"locate\"></ion-icon>\r\n          Ubicación\r\n        </ion-button>\r\n    </ion-col>\r\n  </ion-row>  \r\n</ion-header>\r\n<ion-content> \r\n  <ion-fab vertical=\"bottom\" horizontal=\"start\" slot=\"fixed\">\r\n    <ion-fab-button (click)=\"ShowCords()\" ion-fab color=\"tertiary\">\r\n      <ion-icon name=\"information-circle-outline\"></ion-icon>\r\n    </ion-fab-button>\r\n  </ion-fab>\r\n  <div class=\"map-wrapper\" style=\"height: 100%;\">      \r\n    <div id=\"map_center\">\r\n      <ion-icon name=\"pin\" size=\"large\" color=\"danger\"></ion-icon>      \r\n    </div>\r\n    <div #map id=\"map\"  style=\"height: 100%;\"></div>\r\n  </div>   \r\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/ubicacion/ubicacion-routing.module.ts":
    /*!*******************************************************!*\
      !*** ./src/app/ubicacion/ubicacion-routing.module.ts ***!
      \*******************************************************/

    /*! exports provided: UbicacionPageRoutingModule */

    /***/
    function srcAppUbicacionUbicacionRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UbicacionPageRoutingModule", function () {
        return UbicacionPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ubicacion_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./ubicacion.page */
      "./src/app/ubicacion/ubicacion.page.ts");

      var routes = [{
        path: '',
        component: _ubicacion_page__WEBPACK_IMPORTED_MODULE_3__["UbicacionPage"]
      }];

      var UbicacionPageRoutingModule = function UbicacionPageRoutingModule() {
        _classCallCheck(this, UbicacionPageRoutingModule);
      };

      UbicacionPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], UbicacionPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/ubicacion/ubicacion.module.ts":
    /*!***********************************************!*\
      !*** ./src/app/ubicacion/ubicacion.module.ts ***!
      \***********************************************/

    /*! exports provided: UbicacionPageModule */

    /***/
    function srcAppUbicacionUbicacionModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UbicacionPageModule", function () {
        return UbicacionPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _ubicacion_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./ubicacion-routing.module */
      "./src/app/ubicacion/ubicacion-routing.module.ts");
      /* harmony import */


      var _ubicacion_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./ubicacion.page */
      "./src/app/ubicacion/ubicacion.page.ts");

      var UbicacionPageModule = function UbicacionPageModule() {
        _classCallCheck(this, UbicacionPageModule);
      };

      UbicacionPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _ubicacion_routing_module__WEBPACK_IMPORTED_MODULE_5__["UbicacionPageRoutingModule"]],
        declarations: [_ubicacion_page__WEBPACK_IMPORTED_MODULE_6__["UbicacionPage"]]
      })], UbicacionPageModule);
      /***/
    },

    /***/
    "./src/app/ubicacion/ubicacion.page.scss":
    /*!***********************************************!*\
      !*** ./src/app/ubicacion/ubicacion.page.scss ***!
      \***********************************************/

    /*! exports provided: default */

    /***/
    function srcAppUbicacionUbicacionPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "#map_canvas {\n  width: 90%;\n  height: 80%;\n  border: 1px solid red;\n}\n\n#address {\n  padding: 10px;\n  font-size: 18px;\n  font-weight: bold;\n}\n\n#map {\n  width: 100%;\n  height: 100px;\n}\n\n.map-wrapper {\n  position: relative;\n}\n\n.map-wrapper #map_center {\n  position: absolute;\n  z-index: 99;\n  height: 40px;\n  width: 40px;\n  top: 50%;\n  left: 50%;\n  margin-left: -21px;\n  margin-top: -41px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdWJpY2FjaW9uL3ViaWNhY2lvbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxVQUFBO0VBQ0EsV0FBQTtFQUNBLHFCQUFBO0FBQ0o7O0FBR0U7RUFDRSxhQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FBQUo7O0FBR0U7RUFDRSxXQUFBO0VBQ0EsYUFBQTtBQUFKOztBQUdFO0VBQ0Usa0JBQUE7QUFBSjs7QUFFSTtFQUNFLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0FBQU4iLCJmaWxlIjoic3JjL2FwcC91YmljYWNpb24vdWJpY2FjaW9uLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiNtYXBfY2FudmFzIHtcclxuICAgIHdpZHRoOiA5MCU7XHJcbiAgICBoZWlnaHQ6IDgwJTtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkIHJlZDtcclxuICB9XHJcbiAgIFxyXG4gICBcclxuICAjYWRkcmVzcyB7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgfVxyXG4gICBcclxuICAjbWFwIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDBweDtcclxuICB9XHJcbiAgIFxyXG4gIC5tYXAtd3JhcHBlciB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgIFxyXG4gICAgI21hcF9jZW50ZXIge1xyXG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgIHotaW5kZXg6IDk5O1xyXG4gICAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICAgIHdpZHRoOiA0MHB4O1xyXG4gICAgICB0b3A6IDUwJTtcclxuICAgICAgbGVmdDogNTAlO1xyXG4gICAgICBtYXJnaW4tbGVmdDogLTIxcHg7XHJcbiAgICAgIG1hcmdpbi10b3A6IC00MXB4O1xyXG4gICAgfVxyXG4gIH0iXX0= */";
      /***/
    },

    /***/
    "./src/app/ubicacion/ubicacion.page.ts":
    /*!*********************************************!*\
      !*** ./src/app/ubicacion/ubicacion.page.ts ***!
      \*********************************************/

    /*! exports provided: UbicacionPage */

    /***/
    function srcAppUbicacionUbicacionPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UbicacionPage", function () {
        return UbicacionPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic-native/geolocation/ngx */
      "./node_modules/@ionic-native/geolocation/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic-native/native-geocoder/ngx */
      "./node_modules/@ionic-native/native-geocoder/__ivy_ngcc__/ngx/index.js"); //IMPORTAR LOS MODULOS NECESARIOS PARA LAS FUNCIONES.


      var UbicacionPage = /*#__PURE__*/function () {
        function UbicacionPage(geolocation, nativeGeocoder, zone) {
          _classCallCheck(this, UbicacionPage);

          this.geolocation = geolocation;
          this.nativeGeocoder = nativeGeocoder;
          this.zone = zone;
          this.GoogleAutocomplete = new google.maps.places.AutocompleteService();
          this.autocomplete = {
            input: ''
          };
          this.autocompleteItems = [];
        }

        _createClass(UbicacionPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.loadMap();
          } //CARGAR EL MAPA TIENE DOS PARTES 

        }, {
          key: "loadMap",
          value: function loadMap() {
            var _this = this;

            //OBTENEMOS LAS COORDENADAS DESDE EL TELEFONO.
            this.geolocation.getCurrentPosition().then(function (resp) {
              var latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
              var mapOptions = {
                center: latLng,
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP
              }; //CUANDO TENEMOS LAS COORDENADAS SIMPLEMENTE NECESITAMOS PASAR AL MAPA DE GOOGLE TODOS LOS PARAMETROS.

              _this.getAddressFromCoords(resp.coords.latitude, resp.coords.longitude);

              _this.map = new google.maps.Map(_this.mapElement.nativeElement, mapOptions);

              _this.map.addListener('tilesloaded', function () {
                console.log('accuracy', _this.map, _this.map.center.lat());

                _this.getAddressFromCoords(_this.map.center.lat(), _this.map.center.lng());

                _this.lat = _this.map.center.lat();
                _this["long"] = _this.map.center.lng();
              });
            })["catch"](function (error) {
              console.log('Error getting location', error);
            });
          }
        }, {
          key: "getAddressFromCoords",
          value: function getAddressFromCoords(lattitude, longitude) {
            var _this2 = this;

            console.log("getAddressFromCoords " + lattitude + " " + longitude);
            var options = {
              useLocale: true,
              maxResults: 5
            };
            this.nativeGeocoder.reverseGeocode(lattitude, longitude, options).then(function (result) {
              _this2.address = "";
              var responseAddress = [];

              for (var _i = 0, _Object$entries = Object.entries(result[0]); _i < _Object$entries.length; _i++) {
                var _Object$entries$_i = _slicedToArray(_Object$entries[_i], 2),
                    key = _Object$entries$_i[0],
                    value = _Object$entries$_i[1];

                if (value.length > 0) responseAddress.push(value);
              }

              responseAddress.reverse();

              for (var _i2 = 0, _responseAddress = responseAddress; _i2 < _responseAddress.length; _i2++) {
                var _value = _responseAddress[_i2];
                _this2.address += _value + ", ";
              }

              _this2.address = _this2.address.slice(0, -2);
            })["catch"](function (error) {
              _this2.address = "Address Not Available!";
            });
          } //FUNCION DEL BOTON INFERIOR PARA QUE NOS DIGA LAS COORDENADAS DEL LUGAR EN EL QUE POSICIONAMOS EL PIN.

        }, {
          key: "ShowCords",
          value: function ShowCords() {
            alert('lat' + this.lat + ', long' + this["long"]);
          } //AUTOCOMPLETE, SIMPLEMENTE ACTUALIZAMOS LA LISTA CON CADA EVENTO DE ION CHANGE EN LA VISTA.

        }, {
          key: "UpdateSearchResults",
          value: function UpdateSearchResults() {
            var _this3 = this;

            if (this.autocomplete.input == '') {
              this.autocompleteItems = [];
              return;
            }

            this.GoogleAutocomplete.getPlacePredictions({
              input: this.autocomplete.input
            }, function (predictions, status) {
              _this3.autocompleteItems = [];

              _this3.zone.run(function () {
                predictions.forEach(function (prediction) {
                  _this3.autocompleteItems.push(prediction);
                });
              });
            });
          } //FUNCION QUE LLAMAMOS DESDE EL ITEM DE LA LISTA.

        }, {
          key: "SelectSearchResult",
          value: function SelectSearchResult(item) {
            //AQUI PONDREMOS LO QUE QUERAMOS QUE PASE CON EL PLACE ESCOGIDO, GUARDARLO, SUBIRLO A FIRESTORE.
            //HE AÑADIDO UN ALERT PARA VER EL CONTENIDO QUE NOS OFRECE GOOGLE Y GUARDAMOS EL PLACEID PARA UTILIZARLO POSTERIORMENTE SI QUEREMOS.
            alert(JSON.stringify(item));
            this.placeid = item.place_id;
          } //LLAMAMOS A ESTA FUNCION PARA LIMPIAR LA LISTA CUANDO PULSAMOS IONCLEAR.

        }, {
          key: "ClearAutocomplete",
          value: function ClearAutocomplete() {
            this.autocompleteItems = [];
            this.autocomplete.input = '';
          } //EJEMPLO PARA IR A UN LUGAR DESDE UN LINK EXTERNO, ABRIR GOOGLE MAPS PARA DIRECCIONES. 

        }, {
          key: "GoTo",
          value: function GoTo() {
            return window.location.href = 'https://www.google.com/maps/search/?api=1&query=Google&query_place_id=' + this.placeid;
          }
        }]);

        return UbicacionPage;
      }();

      UbicacionPage.ctorParameters = function () {
        return [{
          type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_2__["Geolocation"]
        }, {
          type: _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_3__["NativeGeocoder"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"]
        }];
      };

      UbicacionPage.propDecorators = {
        mapElement: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: ['map', {
            "static": false
          }]
        }]
      };
      UbicacionPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-ubicacion',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./ubicacion.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/ubicacion/ubicacion.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./ubicacion.page.scss */
        "./src/app/ubicacion/ubicacion.page.scss"))["default"]]
      })], UbicacionPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=ubicacion-ubicacion-module-es5.js.map