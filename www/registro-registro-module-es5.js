(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["registro-registro-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/registro/registro.page.html":
    /*!***********************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/registro/registro.page.html ***!
      \***********************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppRegistroRegistroPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content color=\"backgroundLogin\">\r\n  <div class=\"header\">\r\n    <ion-toolbar>\r\n      <div class=\"img-container\">\r\n        <ion-back-button slot=\"start\" defaultHref=\"login\"></ion-back-button>\r\n        <img class=\"img-food\" src=\"../../assets/imagenes/fondoMain.png\" />\r\n      </div>\r\n    </ion-toolbar>\r\n  </div>\r\n  <div class=\"ion-text-center\" class=\"container-input\">    \r\n      <!-- Registro -->\r\n      <div class=\"backgroundInput\">\r\n        <div class=\"container-inputs\" style=\"margin-bottom: 50px\">\r\n          <ion-label class=\"label\">Tipo de usuario</ion-label>\r\n          <ion-item>\r\n            \r\n            <ion-select   [(ngModel)]=\"user.role\"  value=\"cliente\" okText=\"Aceptar\" cancelText=\"Cancelar\">\r\n              <ion-select-option value=\"paciente\">Paciente</ion-select-option>\r\n              <ion-select-option value=\"Familiar\">Familiar</ion-select-option>\r\n            </ion-select>\r\n          </ion-item>\r\n        </div>   \r\n        <div class=\"container-inputs\" style=\"margin-bottom: 50px\">\r\n          <ion-label class=\"label\">Nombre:</ion-label>\r\n          <ion-item>\r\n            <ion-input name=\"password\" [(ngModel)]=\"user.name\" name=\"name\" type=\"text\"></ion-input>\r\n          </ion-item>\r\n        </div>\r\n        <div class=\"container-inputs\" style=\"margin-bottom: 50px\">\r\n          <ion-label class=\"label\">Correo electrónico</ion-label>\r\n          <ion-item>\r\n            <ion-input name=\"password\" [(ngModel)]=\"user.email\" name=\"email\" type=\"email\"></ion-input>\r\n          </ion-item>\r\n        </div>\r\n        <div class=\"container-inputs\">\r\n          <ion-label class=\"label\">Contraseña</ion-label>\r\n          <ion-item>\r\n            <ion-input [type]=\"password_type\" name=\"password\" [(ngModel)]=\"user.password\"></ion-input>\r\n            <ion-icon [name]=\"eye_type\" item-right (click)=\"togglePasswordMode()\"></ion-icon>\r\n          </ion-item>         \r\n        </div>  \r\n        <div class=\"container-btn\">\r\n          <button (click)=\"signin()\" class=\"btn\">Regístrate</button>\r\n        </div>\r\n    </div>\r\n  </div>\r\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/registro/registro-routing.module.ts":
    /*!*****************************************************!*\
      !*** ./src/app/registro/registro-routing.module.ts ***!
      \*****************************************************/

    /*! exports provided: RegistroPageRoutingModule */

    /***/
    function srcAppRegistroRegistroRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RegistroPageRoutingModule", function () {
        return RegistroPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _registro_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./registro.page */
      "./src/app/registro/registro.page.ts");

      var routes = [{
        path: '',
        component: _registro_page__WEBPACK_IMPORTED_MODULE_3__["RegistroPage"]
      }];

      var RegistroPageRoutingModule = function RegistroPageRoutingModule() {
        _classCallCheck(this, RegistroPageRoutingModule);
      };

      RegistroPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], RegistroPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/registro/registro.module.ts":
    /*!*********************************************!*\
      !*** ./src/app/registro/registro.module.ts ***!
      \*********************************************/

    /*! exports provided: RegistroPageModule */

    /***/
    function srcAppRegistroRegistroModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RegistroPageModule", function () {
        return RegistroPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _registro_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./registro-routing.module */
      "./src/app/registro/registro-routing.module.ts");
      /* harmony import */


      var _registro_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./registro.page */
      "./src/app/registro/registro.page.ts");

      var RegistroPageModule = function RegistroPageModule() {
        _classCallCheck(this, RegistroPageModule);
      };

      RegistroPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _registro_routing_module__WEBPACK_IMPORTED_MODULE_5__["RegistroPageRoutingModule"]],
        declarations: [_registro_page__WEBPACK_IMPORTED_MODULE_6__["RegistroPage"]]
      })], RegistroPageModule);
      /***/
    },

    /***/
    "./src/app/registro/registro.page.scss":
    /*!*********************************************!*\
      !*** ./src/app/registro/registro.page.scss ***!
      \*********************************************/

    /*! exports provided: default */

    /***/
    function srcAppRegistroRegistroPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-content {\n  --padding-top: 0px;\n  --padding-start: 0px;\n}\n\nion-toolbar {\n  --padding-top:0px;\n  --padding-start:0px;\n  --padding-bottom:0px;\n}\n\nion-segment-button {\n  --color-checked:#088bb3;\n  --border-style:none;\n}\n\n.header {\n  border-radius: 0px 0px 30px 30px;\n  background-color: white;\n  height: 360px;\n  overflow: hidden;\n}\n\n.img-container {\n  text-align: center;\n  width: 100%;\n}\n\n.img-food {\n  width: 292px;\n  height: 300px;\n}\n\n.backgroundInput {\n  color: black;\n  margin-top: 50px;\n}\n\n.container-input {\n  padding: 30px;\n}\n\nion-datetime {\n  border-bottom: 0.5px solid #000000;\n}\n\n.container-inputs {\n  margin-bottom: 10px;\n}\n\n.label {\n  color: black;\n  opacity: 0.4;\n}\n\n.container-btn {\n  display: flex;\n  justify-content: center;\n  margin-top: 20%;\n  width: 100%;\n}\n\n.my-custom-class {\n  --background:#04f4cc;\n  --spinner-color:#188088;\n}\n\nion-select {\n  width: 100%;\n  justify-content: center;\n}\n\n.btn {\n  width: 90%;\n  height: 70px;\n  background: #088bb3;\n  box-shadow: 0px 4px 4px rgba(0, 102, 255, 0.5);\n  border-radius: 30px;\n  color: white;\n  font-size: 17px;\n}\n\nion-item {\n  --background: #F2F2F2;\n  --highlight-color-focused:#088bb3;\n  --highlight-color-valid:#088bb3;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmVnaXN0cm8vcmVnaXN0cm8ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksa0JBQUE7RUFDQSxvQkFBQTtBQUNKOztBQUVBO0VBQ0ksaUJBQUE7RUFDQSxtQkFBQTtFQUNBLG9CQUFBO0FBQ0o7O0FBQ0E7RUFDSSx1QkFBQTtFQUNBLG1CQUFBO0FBRUo7O0FBQ0E7RUFDSSxnQ0FBQTtFQUNBLHVCQUFBO0VBQ0EsYUFBQTtFQUNBLGdCQUFBO0FBRUo7O0FBQ0E7RUFDSSxrQkFBQTtFQUVBLFdBQUE7QUFDSjs7QUFFQTtFQUNJLFlBQUE7RUFDQSxhQUFBO0FBQ0o7O0FBRUE7RUFDSSxZQUFBO0VBQ0EsZ0JBQUE7QUFDSjs7QUFFQTtFQUNJLGFBQUE7QUFDSjs7QUFHQTtFQUNJLGtDQUFBO0FBQUo7O0FBR0E7RUFDSSxtQkFBQTtBQUFKOztBQUdBO0VBQ0ksWUFBQTtFQUNBLFlBQUE7QUFBSjs7QUFHQTtFQUNJLGFBQUE7RUFDQSx1QkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0FBQUo7O0FBSUE7RUFDRSxvQkFBQTtFQUNFLHVCQUFBO0FBREo7O0FBSUU7RUFDRSxXQUFBO0VBRUEsdUJBQUE7QUFGSjs7QUFNQTtFQUNJLFVBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSw4Q0FBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7QUFISjs7QUFNQTtFQUNJLHFCQUFBO0VBQ0EsaUNBQUE7RUFDQSwrQkFBQTtBQUhKIiwiZmlsZSI6InNyYy9hcHAvcmVnaXN0cm8vcmVnaXN0cm8ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQge1xyXG4gICAgLS1wYWRkaW5nLXRvcDogMHB4O1xyXG4gICAgLS1wYWRkaW5nLXN0YXJ0OiAwcHg7XHJcbn1cclxuXHJcbmlvbi10b29sYmFyIHtcclxuICAgIC0tcGFkZGluZy10b3A6MHB4O1xyXG4gICAgLS1wYWRkaW5nLXN0YXJ0OjBweDtcclxuICAgIC0tcGFkZGluZy1ib3R0b206MHB4O1xyXG59XHJcbmlvbi1zZWdtZW50LWJ1dHRvbntcclxuICAgIC0tY29sb3ItY2hlY2tlZDojMDg4YmIzO1xyXG4gICAgLS1ib3JkZXItc3R5bGU6bm9uZTtcclxufVxyXG5cclxuLmhlYWRlcntcclxuICAgIGJvcmRlci1yYWRpdXM6IDBweCAwcHggMzBweCAzMHB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgICBoZWlnaHQ6IDM2MHB4O1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxufVxyXG5cclxuLmltZy1jb250YWluZXIge1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgXHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLmltZy1mb29kIHtcclxuICAgIHdpZHRoOiAyOTJweDtcclxuICAgIGhlaWdodDogMzAwcHg7XHJcbn1cclxuXHJcbi5iYWNrZ3JvdW5kSW5wdXQge1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG4gICAgbWFyZ2luLXRvcDogNTBweDtcclxufVxyXG5cclxuLmNvbnRhaW5lci1pbnB1dCB7XHJcbiAgICBwYWRkaW5nOiAzMHB4O1xyXG59XHJcblxyXG5cclxuaW9uLWRhdGV0aW1le1xyXG4gICAgYm9yZGVyLWJvdHRvbTogMC41cHggc29saWQgIzAwMDAwMDtcclxufVxyXG5cclxuLmNvbnRhaW5lci1pbnB1dHMge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxufVxyXG5cclxuLmxhYmVsIHtcclxuICAgIGNvbG9yOiBibGFjaztcclxuICAgIG9wYWNpdHk6IDAuNDtcclxufVxyXG5cclxuLmNvbnRhaW5lci1idG4ge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgbWFyZ2luLXRvcDogMjAlO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgIFxyXG59XHJcbi8vY29sb3IgZGUgbG9hZGluZ1xyXG4ubXktY3VzdG9tLWNsYXNzIHtcclxuICAtLWJhY2tncm91bmQ6IzA0ZjRjYztcclxuICAgIC0tc3Bpbm5lci1jb2xvcjojMTg4MDg4O1xyXG4gIH1cclxuXHJcbiAgaW9uLXNlbGVjdCB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICBcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIH1cclxuXHJcbiAgXHJcbi5idG4ge1xyXG4gICAgd2lkdGg6IDkwJTtcclxuICAgIGhlaWdodDogNzBweDtcclxuICAgIGJhY2tncm91bmQ6ICMwODhiYjM7XHJcbiAgICBib3gtc2hhZG93OiAwcHggNHB4IDRweCByZ2JhKDAsIDEwMiwgMjU1LCAwLjUpO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMzBweDtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGZvbnQtc2l6ZTogMTdweDtcclxufVxyXG5cclxuaW9uLWl0ZW17XHJcbiAgICAtLWJhY2tncm91bmQ6ICNGMkYyRjI7XHJcbiAgICAtLWhpZ2hsaWdodC1jb2xvci1mb2N1c2VkOiMwODhiYjM7XHJcbiAgICAtLWhpZ2hsaWdodC1jb2xvci12YWxpZDojMDg4YmIzO1xyXG59Il19 */";
      /***/
    },

    /***/
    "./src/app/registro/registro.page.ts":
    /*!*******************************************!*\
      !*** ./src/app/registro/registro.page.ts ***!
      \*******************************************/

    /*! exports provided: RegistroPage */

    /***/
    function srcAppRegistroRegistroPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RegistroPage", function () {
        return RegistroPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../services/auth.service */
      "./src/app/services/auth.service.ts");
      /* harmony import */


      var firebase_auth__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! firebase/auth */
      "./node_modules/firebase/auth/dist/index.esm.js");

      var RegistroPage = /*#__PURE__*/function () {
        function RegistroPage(authService, alertCtrl, router, loadingController) {
          _classCallCheck(this, RegistroPage);

          this.authService = authService;
          this.alertCtrl = alertCtrl;
          this.router = router;
          this.loadingController = loadingController;
          this.isActiveToggleTextPassword = true;
          this.password_type = 'password';
          this.eye_type = 'eye';
          this.user = {
            name: '',
            typeid: '',
            nroid: '',
            gender: '',
            role: 'cliente',
            address: '',
            birthdaydate: '',
            email: '',
            password: '',
            confpassword: '',
            category: '',
            number: '',
            img: 'https://firebasestorage.googleapis.com/v0/b/palmago-45e5a.appspot.com/o/default-user.png?alt=media&token=f3e1a1a3-9532-48ac-a2aa-ebaeca5f5ce2',
            lat: '',
            log: ''
          };
        } //Visualizar contrase 


        _createClass(RegistroPage, [{
          key: "togglePasswordMode",
          value: function togglePasswordMode() {
            this.eye_type = this.eye_type === 'eye' ? 'eye-off' : 'eye';
            this.password_type = this.password_type === 'text' ? 'password' : 'text';
          } //Inicar seccion  

        }, {
          key: "loginUser",
          value: function loginUser() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var _this = this;

              var loading;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      if (!(this.user.email != '' && this.user.password != '')) {
                        _context2.next = 10;
                        break;
                      }

                      _context2.next = 3;
                      return this.loadingController.create({
                        cssClass: 'my-custom-class',
                        message: 'Por  favor espere...'
                      });

                    case 3:
                      loading = _context2.sent;
                      _context2.next = 6;
                      return loading.present();

                    case 6:
                      console.log("Ingreso");
                      this.authService.loginUser(this.user.email, this.user.password).then(function () {
                        loading.dismiss();

                        _this.router.navigateByUrl('main-menu');
                      }, function (error) {
                        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                          var alert;
                          return regeneratorRuntime.wrap(function _callee$(_context) {
                            while (1) {
                              switch (_context.prev = _context.next) {
                                case 0:
                                  loading.dismiss();
                                  _context.next = 3;
                                  return this.alertCtrl.create({
                                    message: "No hay ningún registro de usuario que corresponda a este identificador.",
                                    buttons: [{
                                      text: 'Aceptar',
                                      role: 'cancel'
                                    }]
                                  });

                                case 3:
                                  alert = _context.sent;
                                  _context.next = 6;
                                  return alert.present();

                                case 6:
                                case "end":
                                  return _context.stop();
                              }
                            }
                          }, _callee, this);
                        }));
                      });
                      _context2.next = 11;
                      break;

                    case 10:
                      this.error("Inicio sesión", "Por favor ingrese todos los campos");

                    case 11:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          } //Registrarse usuarios

        }, {
          key: "signin",
          value: function signin() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
              var _this2 = this;

              var loading;
              return regeneratorRuntime.wrap(function _callee4$(_context4) {
                while (1) {
                  switch (_context4.prev = _context4.next) {
                    case 0:
                      _context4.next = 2;
                      return this.loadingController.create({
                        cssClass: 'my-custom-class',
                        message: 'Por  favor espere...'
                      });

                    case 2:
                      loading = _context4.sent;

                      if (!(this.user.email != '' && this.user.password != '' && this.user.name != '' && this.user.role != '')) {
                        _context4.next = 13;
                        break;
                      }

                      if (!this.validar_clave(this.user.password)) {
                        _context4.next = 10;
                        break;
                      }

                      _context4.next = 7;
                      return loading.present();

                    case 7:
                      this.authService.signupUser(this.user.name, this.user.typeid, this.user.nroid, this.user.gender, this.user.role, this.user.address, this.user.birthdaydate, this.user.number, this.user.email, this.user.password, this.user.img, this.user.category, this.user.lat, this.user.log).then(function () {
                        loading.dismiss();

                        _this2.router.navigateByUrl('login');

                        _this2.type = "login";
                      }, function (error) {
                        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
                          var alert;
                          return regeneratorRuntime.wrap(function _callee3$(_context3) {
                            while (1) {
                              switch (_context3.prev = _context3.next) {
                                case 0:
                                  loading.dismiss();
                                  _context3.next = 3;
                                  return this.alertCtrl.create({
                                    message: "La dirección de correo electrónico ya está siendo utilizada por otra cuenta.",
                                    buttons: [{
                                      text: 'Aceptar',
                                      role: 'cancel'
                                    }]
                                  });

                                case 3:
                                  alert = _context3.sent;
                                  console.log("error " + error);
                                  _context3.next = 7;
                                  return alert.present();

                                case 7:
                                case "end":
                                  return _context3.stop();
                              }
                            }
                          }, _callee3, this);
                        }));
                      });
                      _context4.next = 11;
                      break;

                    case 10:
                      this.error("Registro", "Tu contraseña debe tener al menos 8 letras, numeros y simbolos como (! y &)");

                    case 11:
                      _context4.next = 15;
                      break;

                    case 13:
                      loading.dismiss();
                      this.error("Registro", "Por favor ingrese todos los campos");

                    case 15:
                    case "end":
                      return _context4.stop();
                  }
                }
              }, _callee4, this);
            }));
          }
        }, {
          key: "validar_clave",
          value: function validar_clave(contrasenna) {
            if (contrasenna.length >= 8) {
              var mayuscula = false;
              var minuscula = false;
              var numero = false;
              var caracter_raro = false;

              for (var i = 0; i < contrasenna.length; i++) {
                if (contrasenna.charCodeAt(i) >= 65 && contrasenna.charCodeAt(i) <= 90) {
                  mayuscula = true;
                } else if (contrasenna.charCodeAt(i) >= 97 && contrasenna.charCodeAt(i) <= 122) {
                  minuscula = true;
                } else if (contrasenna.charCodeAt(i) >= 48 && contrasenna.charCodeAt(i) <= 57) {
                  numero = true;
                } else {
                  caracter_raro = true;
                }
              }

              if (mayuscula == true && minuscula == true && caracter_raro == true && numero == true) {
                return true;
              }
            }

            return false;
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {
            //Variable para iniciar con el  ion-segment
            this.type = "login";
          }
        }, {
          key: "segmentChanged",
          value: function segmentChanged(ev) {
            //valor que se escoge cuando se cambia la opcion en  ion-segment
            console.log('Segment changed', ev);
          } //Mensaje de error

        }, {
          key: "error",
          value: function error(title, messag) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
              var alert;
              return regeneratorRuntime.wrap(function _callee5$(_context5) {
                while (1) {
                  switch (_context5.prev = _context5.next) {
                    case 0:
                      _context5.next = 2;
                      return this.alertCtrl.create({
                        message: messag,
                        subHeader: title,
                        buttons: [{
                          text: 'Aceptar',
                          role: 'cancel'
                        }]
                      });

                    case 2:
                      alert = _context5.sent;
                      _context5.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context5.stop();
                  }
                }
              }, _callee5, this);
            }));
          }
        }]);

        return RegistroPage;
      }();

      RegistroPage.ctorParameters = function () {
        return [{
          type: _services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"]
        }];
      };

      RegistroPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-registro',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./registro.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/registro/registro.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./registro.page.scss */
        "./src/app/registro/registro.page.scss"))["default"]]
      })], RegistroPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=registro-registro-module-es5.js.map